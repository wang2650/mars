﻿using Cdy.Tag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBDevelopService
{
    public class DbManager
    {

        #region ... Variables  ...
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, Cdy.Tag.Database> mDatabase = new Dictionary<string, Cdy.Tag.Database>();

        public static DbManager Instance = new DbManager();

        #endregion ...Variables...

        #region ... Events     ...

        #endregion ...Events...

        #region ... Constructor...

        #endregion ...Constructor...

        #region ... Properties ...

        /// <summary>
        /// 
        /// </summary>
        public bool IsLoaded { get; set; }

        #endregion ...Properties...

        #region ... Methods    ...

        /// <summary>
        /// 
        /// </summary>
        public void Load()
        {
            string databasePath = PathHelper.helper.DataPath;
            
            if (System.IO.Directory.Exists(databasePath))
            {
                foreach (var vv in System.IO.Directory.EnumerateDirectories(databasePath))
                {
                    string sname = new System.IO.DirectoryInfo(vv).Name;

                    Cdy.Tag.Database db = new Cdy.Tag.DatabaseSerise().Load(sname);
                    
                    if (string.IsNullOrEmpty(db.Name)) continue;

                    if (!mDatabase.ContainsKey(db.Name))
                    mDatabase.Add(db.Name, db);
                    else
                    {
                        mDatabase[db.Name] = db;
                    }
                }
            }
            IsLoaded = true;
        }

        /// <summary>
        /// 局部加载
        /// </summary>
        public void PartLoad()
        {
            string databasePath = PathHelper.helper.DataPath;

            if (System.IO.Directory.Exists(databasePath))
            {
                foreach (var vv in System.IO.Directory.EnumerateDirectories(databasePath))
                {
                    string sname = new System.IO.DirectoryInfo(vv).Name;

                    Cdy.Tag.Database db = new Cdy.Tag.DatabaseSerise().PartLoad(sname);
                    if (string.IsNullOrEmpty(db.Name)) continue;
                    if (!mDatabase.ContainsKey(db.Name))
                        mDatabase.Add(db.Name, db);
                    else
                    {
                        mDatabase[db.Name] = db;
                    }
                }
            }
            IsLoaded = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="db"></param>
        public void CheckAndContinueLoadDatabase(Database db)
        {
            lock (db)
            {
                if (db.RealDatabase == null)
                {
                    new Cdy.Tag.DatabaseSerise() { Dbase = db }.ContinuePartLoad(db.Name);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void QuickReload()
        {
            string databasePath = PathHelper.helper.DataPath;

            if (System.IO.Directory.Exists(databasePath))
            {
                foreach (var vv in System.IO.Directory.EnumerateDirectories(databasePath))
                {
                    string sname = new System.IO.DirectoryInfo(vv).Name;
                    if (!mDatabase.ContainsKey(sname))
                    {
                        Cdy.Tag.Database db = new Cdy.Tag.DatabaseSerise().Load(sname);
                        if (string.IsNullOrEmpty(db.Name)) continue;
                        if (!mDatabase.ContainsKey(sname))
                            mDatabase.Add(db.Name, db);
                        else
                        {
                            mDatabase[sname] = db;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="database"></param>
        public void ReLoad(string database)
        {
            string databasePath = PathHelper.helper.DataPath;

            if (System.IO.Directory.Exists(databasePath))
            {
                foreach (var vv in System.IO.Directory.EnumerateDirectories(databasePath))
                {
                    string sname = new System.IO.DirectoryInfo(vv).Name;

                    if (sname == database)
                    {
                        Cdy.Tag.Database db = new Cdy.Tag.DatabaseSerise().Load(sname);

                        if (string.IsNullOrEmpty(db.Name)) continue;

                        if (!mDatabase.ContainsKey(db.Name))
                            mDatabase.Add(db.Name, db);
                        else
                        {
                            mDatabase[db.Name] = db;
                        }
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="db"></param>
        public void AddDatabase(string name,Database db)
        {
            if(!mDatabase.ContainsKey(name))
            {
                mDatabase[name] = db;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Reload()
        {
            mDatabase.Clear();
            Load();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="database"></param>
        public void Reload(string database)
        {
            if(mDatabase.ContainsKey(database))
            {
                mDatabase.Remove(database);
            }

            Cdy.Tag.Database db = new Cdy.Tag.DatabaseSerise().Load(database);
            if (!mDatabase.ContainsKey(db.Name))
                mDatabase.Add(db.Name, db);
            else
            {
                mDatabase[db.Name] = db;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public Database NewDB(string name,string desc)
        {
            if (mDatabase.ContainsKey(name))
            {
                mDatabase[name] = new Cdy.Tag.Database() { Name = name,Desc=desc };
            }
            else
            {
                mDatabase.Add(name, new Cdy.Tag.Database() { Name = name,Desc=desc });
            }
            Save(mDatabase[name]);
            return mDatabase[name];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        /// <returns></returns>
        public bool RemoveDB(string name)
        {
            if (mDatabase.ContainsKey(name))
            {
                CheckAndContinueLoadDatabase(mDatabase[name]);
                CheckAndRemoveDatabaseFile(mDatabase[name]);
                mDatabase.Remove(name);
                return true;
            }
            return false;
        }

        private void CheckAndRemoveDatabaseFile(Cdy.Tag.Database database)
        {
            //删除历史数据
            string spath = database.HisDatabase.Setting.HisDataPathPrimary;
            if(!string.IsNullOrEmpty(spath)&&System.IO.Directory.Exists(spath))
            {
                System.IO.Directory.Delete(spath, true);
            }

            //删除备份历史数据
            spath = database.HisDatabase.Setting.HisDataPathBack;
            if (!string.IsNullOrEmpty(spath) && System.IO.Directory.Exists(spath))
            {
                System.IO.Directory.Delete(spath, true);
            }

            //删除所有配置文件
            spath = PathHelper.helper.GetDataPath(database.Name, "");
            if(System.IO.Directory.Exists(spath))
            {
                System.IO.Directory.Delete(spath,true);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Cdy.Tag.Database GetDatabase(string name)
        {
            if (mDatabase.ContainsKey(name))
                return mDatabase[name];
            else
                return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="database"></param>
        /// <returns></returns>
        public bool Save(Cdy.Tag.Database database)
        {
            try
            {
                new Cdy.Tag.DatabaseSerise() { Dbase = database }.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string[] ListDatabase()
        {
            return mDatabase.Keys.ToArray();
        }

        #endregion ...Methods...

        #region ... Interfaces ...

        #endregion ...Interfaces...
    }
}
